import pandas as pd
import logging
import requests
import json
import sys
from functools import reduce

CONFIG_FILEPATH = "config.json"
config = json.load(open(CONFIG_FILEPATH, 'r'))

from utils import map_record, getLogger

logger = getLogger(service="salesforce_update", source="python_scripts", name=__name__)

ENV = 'I3'

def get_sf_connection(ENV='I3'):
    config_sf = config['SF'][ENV]
    params = {
        "grant_type": "password",
        "client_id": config_sf['KEY'], # Consumer Key
        "client_secret": config_sf['SECRET'], # Consumer Secret
        "username": config_sf['LOGIN'], # The email you use to login
        "password": config_sf["PASSWORD"] + config_sf["TOKEN"] # Concat your password and your security token
    }
    try:
        r = requests.post("https://{}.salesforce.com/services/oauth2/token".format('login' if ENV == 'PROD' else 'test'), params=params)
        response = r.json()
        access_token, instance_url = response.get("access_token"), response.get("instance_url")
        logger.info('SF connection success.', extra={"type": "SF connection"})
        return access_token, instance_url
    except:
        logger.error('SF connection Failed.')

def get_all(sf_object, ENV='I3'):
    query_filepath = 'queries/salesforce/{}.txt'.format(sf_object.lower())
    with open(query_filepath, 'r') as f:
        query = f.read().replace('\n', ' ')
        token, url = get_sf_connection(ENV)
        n_url = "/services/data/v50.0/queryAll/?q=" + query
        df = []
        try:
            logger.info("requesting all {} records from SF (ENV:{})...".format(sf_object, ENV), extra={"type": "SF request", "sf_object": sf_object})
            while n_url:
                r = requests.get(url + n_url,  headers={"Authorization":"Bearer " + token})
                response = r.json()
                df += response.get('records', [])
                n_url = response.get('nextRecordsUrl', False)
            logger.info('success', extra={"type": "SF request", "sf_object": sf_object})
            df = pd.DataFrame(df)
            df.columns = df.columns.str.lower()
            return df
        except:
            logger.error(sys.exc_info(), extra={"level":"error", "type": "SF request", "sf_object": sf_object})
            print(response)
            logger.error('Request Failed.')

def dl_sf_data(ENV='I3'):
    sf_orders = get_all('OPPORTUNITY', ENV=ENV)
    if len(sf_orders) > 0:
        sf_orders.dropna(subset=['legacy_id__c'], inplace=True)
        sf_orders['legacy_id__c'] = sf_orders['legacy_id__c'].astype(int)

    sf_ac = get_all('ACCOUNTING_CODE__C', ENV=ENV).drop_duplicates(subset=['legacy_id__c', 'legacy_erp__c'])
    if len(sf_orders) > 0:
        sf_ac.drop_duplicates(subset=['account__c'], inplace=True)

    sf_contacts = get_all('CONTACT', ENV=ENV)
    sf_lc = get_all('LEGACYCONTACT__C', ENV=ENV)

    if len(sf_contacts) > 0:
        sf_contacts.dropna(subset=['legacy_id__c'], inplace=True)
        sf_contacts['legacy_id__c'] = sf_contacts['legacy_id__c'].astype(float).astype(int)
        sf_contacts = sf_contacts[['legacy_id__c', 'legacy_erp__c', 'id', 'accountid']].dropna(subset=['legacy_id__c'])
        sf_lc = sf_lc[['legacy_id__c', 'legacy_erp__c', 'contact__c', 'account_id_of_the_contact__c']].dropna()
        sf_lc.rename(columns= {'contact__c': 'id', 'account_id_of_the_contact__c': 'accountid'}, inplace=True)
        sf_contacts = pd.concat([sf_contacts, sf_lc]).drop_duplicates(subset=['legacy_erp__c', 'legacy_id__c'])
        sf_contacts['legacy_id__c'] = sf_contacts['legacy_id__c'].apply(lambda x: str(x).split('.')[0])

    sf_ac.to_csv('files/sf_ac.csv', index=False, encoding='utf-8')
    sf_lc.to_csv('files/sf_lc.csv', index=False, encoding='utf-8')
    sf_contacts.to_csv('files/sf_contacts.csv', index=False, encoding='utf-8')
    sf_orders.to_csv('files/sf_orders.csv', index=False, encoding='utf-8')
    return sf_contacts, sf_orders, sf_lc, sf_ac

def read_sf_data(dl=False, ENV='I3'):
    if dl:
        _, _, _, _ = dl_sf_data(ENV=ENV)
    sf_orders = pd.read_csv("files/sf_orders.csv", index_col=False, low_memory=False, encoding='latin-1')
    sf_contacts = pd.read_csv("files/sf_contacts.csv", index_col=False, low_memory=False, encoding='latin-1')
    sf_lc = pd.read_csv("files/sf_lc.csv", index_col=False, low_memory=False, encoding='latin-1')
    sf_ac = pd.read_csv("files/sf_ac.csv", index_col=False, low_memory=False, encoding='latin-1')
    return 

def insert_records(sf_object, record_list, ENV='I3', batch_index=0, n_batch=0, mapping=None):
    endpoint = "/services/data/v34.0/composite/tree/{}/".format(sf_object.lower())
    token, url = get_sf_connection(ENV)
    body = {"records": []}
    mapping = json.load(open('mappings/sf_objects/{}.json'.format(sf_object.lower()), 'r')) if mapping is None else mapping
    body['records'] = list(map(lambda x: map_record(x, mapping, sf_object, x['id']), record_list))
    
    r = requests.post(url + endpoint, 
        headers={"Authorization":"Bearer " + token,
            "Content-Type":"application/json"},
        data=json.dumps(body)
    )
        
    response = r.json()
    try:
        error=False
        if isinstance(response, list):
            logger.error(response, extra={"level": "error"})
            return
        for r in response.get("results", []):
            e = r.get("errors", False)
            if e:
                error = True
                break
        id_list = [r['referenceId'] for r in response.get("results", [])]
        if error:
            print(body["records"])
            logger.error("batch: {}/{}, failure: {}".format(batch_index, n_batch, e), 
                extra={"level": "error", "type": "insertion", "ids": id_list, "sf_object": sf_object})
        else:
            logger.info("batch: {}/{}, success".format(batch_index, n_batch), 
                extra={"level": "info", "type": "insertion", "ids": id_list, "sf_object": sf_object})
    except:
        logger.error("SF HTTP request failed", extra={"level": "error"}, exc_info=True)

def update_records(sf_object, record_list, ENV='I3', batch_index=0, n_batch=0, mapping=None):
    endpoint = "/services/data/v50.0/composite/sobjects/"
    token, url = get_sf_connection(ENV)
    body = {"records": []}
    mapping = json.load(open('mappings/sf_objects/{}.json'.format(sf_object.lower()), 'r')) if mapping is None else mapping
    body['records'] = list(map(lambda x: map_record(x, mapping, sf_object, x['id']), record_list))
    body['allOrNone'] = False
    r = requests.patch(url + endpoint, 
        headers={"Authorization":"Bearer " + token,
            "Content-Type":"application/json"},
        data=json.dumps(body)
    )

    response = r.json()
    try:
        print_body=False
        for r in response:
            e = r.get("errors", False)
            if e:
                print_body = True
                break
        failure_ids = [i.get('id', None) for i in response if not i.get('success', False)]
        success_ids = [i.get('id', None) for i in response if i.get('success', False)]
        if len(failure_ids) > 0:
            logger.error("batch: {}/{}, failure: {}".format(batch_index, n_batch, e), 
                extra={"level": "error", "type": "update", "ids": success_ids, "sf_object": sf_object})
        if len(success_ids) > 0:
            logger.info("batch: {}/{}, success".format(batch_index, n_batch), 
                extra={"level": "info", "type": "update", "ids": success_ids, "sf_object": sf_object})
        if print_body:
            print(body['records'])
    except:
        logger.error(response)
        logger.error(sys.exc_info())
        print(body['records'])


def delete_records(record_list, ENV='I3', batch_index=0, n_batch=0):
    endpoint = "/services/data/v50.0/composite/sobjects?ids={}".format(
        reduce(lambda a,b: a + ',' + b, record_list)
    )
    token, url = get_sf_connection(ENV)

    try:
        r = requests.delete(
            url + endpoint, 
            headers={
                "Authorization":"Bearer " + token,
                "Content-Type":"application/json"
            }
        )
        response = r.json()
        failure_ids = [record_list[ind] for ind, i in enumerate(response) if not i.get('success', False)]
        success_ids = [record_list[ind] for ind, i in enumerate(response) if i.get('success', False)]
        if len(success_ids) > 0:
            logger.info('batch: {}/{}, success'.format(batch_index, n_batch), extra={"level": "info", "type": "deletion", "ids": success_ids})

        for ind, i in enumerate(response):
            if not i.get('success', False):
                logger.error('batch: {}/{}, id: {} error: {}'.format(batch_index, n_batch, record_list[ind], i.get('errors', None)[0].get('message', None)),
                    extra={"type": "deletion", "ids": [record_list[ind]], "level":"error"})   
    except:
        logger.error("SF HTTP Request failed", exc_info=True, extra={"type": "deletion"})

def get_value_set(id):
    endpoint = "/services/data/v41.0/tooling/sobjects/GlobalValueSet/"
    token, url = get_sf_connection()
    request_url = url + endpoint + id
    r = requests.get(request_url, headers={
            "Authorization":"Bearer " + token,
            "Content-Type":"application/json"
        },)
    response = r.json()
    df = pd.DataFrame(response.get('Metadata', {}).get('customValue', []))
    return df

def convert_leads(id_list, ENV='I3'):
    token, url = get_sf_connection(ENV)
    endpoint = "/services/apexrest/AP_MassConvertLeads"
    request_url = url + endpoint
    print(request_url)
    body = {
        "leadIdsWp":[{"leadId": id for id in id_list}]
    }
    logger.info("Converting Leads at {}".format(url),
        extra={"type": "lead convertion", "ids": id_list, "level":"info"})
    r = requests.post(request_url, 
        headers={
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json"
        },
        data=json.dumps(body)
    )
    response = r.json()
    logger.info(response)

if __name__ == '__main__':
    x = get_all('User')
    x['ref'] = x['email'].apply(lambda x: x.split('@')[0])
    x[['ref', 'id']].to_csv('mappings/users.csv', index=False, encoding='latin-1')