import json
import requests
import pypyodbc
import pandas as pd
import logging
import gc

from utils import execute_query_file, format_list
from salesforce import delete_records, get_all

CONFIG_FILEPATH = "config.json"
config = json.load(open(CONFIG_FILEPATH, 'r'))
config_lbs = config['LBS']


def tempo_connection():
    config_tempo = config['TEMPO']
    cnct_str = 'DRIVER={SQL Server}' + ";SERVER={};DATABASE={};UID={};PWD={};".format(
    config_tempo['SERVER'], config_tempo["DB"], config_tempo["USERNAME"], config_tempo["PASSWORD"])
    print(cnct_str)
    try:
        logging.info("Connecting to TEMPO DB...")
        connection = pypyodbc.connect(cnct_str, timeout=10)
        logging.info('Success.')
        return connection
    except pypyodbc.Error as err:
        logging.error('Connection to Tempo DB Failed: {}'.format(err))


if __name__ == '__main__':
    # opps = get_all('OPPORTUNITY', 'PROD')
    # opps = opps[(opps['createdbyid'] == '0055I000001ZjAeQAK') & ~opps['isdeleted']]
    # opps.to_csv('opps.csv', index=False)


    t = tempo_connection()
    df = execute_query_file(t, 'queries/tempo/customers.txt')
    # acs = pd.read_csv("files/acs.csv", index_col=False)
    acs = get_all('ACCOUNT', 'PROD').dropna(subset=['tempo_id__c'])
    # acs[acs['tempo_id__c'].notna() & acs['legal_name__c'].isnull()][['tempo_id__c', 'id']].to_csv('sisi.csv', index=False)

    # contacts = pd.read_csv("files/contacts.csv", index_col=False)
    fi = pd.read_csv("files/fi.csv", index_col=False)
    l = pd.read_csv("files/l.csv", index_col=False)
    lc = pd.read_csv("files/lc.csv", index_col=False)
    gc.collect()
    # l = get_all('LEAD', 'PROD')
    # acs = get_all('ACCOUNT', 'PROD').dropna(subset=['tempo_id__c'])
    # fi = get_all('ACCOUNTING_CODE__C', 'PROD').dropna(subset=['tempo_id__c'])
    contacts = get_all('CONTACT', 'PROD')
    # contacts = contacts[contacts['accountid'] == '0015I000009Emb0QAC']
    print(len(contacts))

    # gc.collect()

    # acs.to_csv('files/acs.csv', index=False)
    # fi.to_csv('files/fi.csv', index=False)
    # contacts.to_csv('files/contacts.csv', index=False)
    # l.to_csv('files/l.csv', index=False)
    # lc.to_csv('files/lc.csv', index=False)
    lc = get_all('LEGACYCONTACT__C', 'PROD')

    df['legacy_id__c'] = df['legacy_id__c'].astype(str)
    df = df.merge(acs[['id', 'tempo_id__c']], how='left', left_on='legacy_id__c', right_on='tempo_id__c')

    lc = lc[lc['legacy_erp__c'] == 'TEMPO TLF']
    contacts['tempo_id__c'] = contacts['tempo_id__c'].apply(lambda x: str(x).split('.')[0])
    contacts_t = contacts[contacts['legacy_erp__c'] == 'TEMPO TLF']
    lc['tempo_id__c'] = lc['legacy_id__c']
    print(lc['tempo_id__c'])
    lc['id'] = lc['contact__c']
    contacts_t = pd.concat([contacts_t, lc])
    contacts_t['tempo_id__c'] = contacts_t['tempo_id__c'].apply(lambda x: str(x).split('.')[0])
    print(len(contacts_t))
    print(len(contacts_t.drop_duplicates(['legacy_id__c'])))

    l['email'] = l['email'].str.strip().str.lower()
    contacts['email'] = contacts['email'].str.strip().str.lower()

    tempo_co = df[['contactid', 'email', 'firstname', 'lastname', 'id', 'legacy_id__c']].drop_duplicates()
    tempo_co = tempo_co.rename(columns={'id':'accountId', 'legacy_id__c': 'account_tempo_id'})
    tempo_co['email'] = tempo_co['email'].str.strip().str.lower()
    tempo_co['contactid'] = tempo_co['contactid'].astype(str)
    tempo_co.to_csv('contacts.csv', index=False)
    tempo_co = tempo_co.merge(contacts_t[['tempo_id__c', 'id']], how='left', left_on='contactid', right_on='tempo_id__c')
    tempo_co = tempo_co[tempo_co['id'].isnull()]
    tempo_co.to_csv('missing_contacts.csv', index=False)
    print(len(tempo_co))
    tempo_co = tempo_co.merge(contacts[['email', 'id', 'legacy_id__c']].drop_duplicates().dropna(subset=['email']), how='left', on='email')
    missing = tempo_co[tempo_co['id_y'].isnull()]
    missing.to_csv('missing_co.csv', index=False)
    existing = tempo_co[tempo_co['id_y'].notna()]
    missing.to_csv('missing.csv', index=False)
    print(len(missing), len(existing))
    
    missing = missing.merge(l[['email', 'id']], how='left', on='email')
    missing = missing[missing['id'].notna()]
    new =  missing[missing['id'].isnull()]
    acs['tempo_id__c'] = acs['tempo_id__c'].astype(str).apply(lambda x: x.split('.')[0])

    contact_ids = missing['contactid'].unique()
    print(contact_ids)
    co = execute_query_file(t, 'queries/tempo/contacts.txt', {"list": format_list(contact_ids)})
    acs = acs[acs['tempo_id__c'].notna()]
    # print(len(acs))
    # co['accountid'] = co['accountid'].astype(str)
    # co = co.merge(acs[['id', 'tempo_id__c']], left_on='accountid', right_on='tempo_id__c')
    # co.to_csv('new_c.csv', index=False)
   


    # print(len(existing), len(new), len(l_to_c))
    # l_to_c.to_csv("l_to_c.csv", index=False, encoding='latin-1')

    existing_lc = existing[existing['legacy_id__c'].notna()]
    existing =  existing[existing['legacy_id__c'].isnull()]
    print(len(existing_lc), len(existing))

    existing.to_csv('existing.csv', index=False)
    existing_lc.to_csv('existing.csv', index=False)

    # l_to<_c[['id']].to_csv('conv.csv', index=False)
    tempo_acs = df[['legacy_erp__c', 'tempo_id__c_x', 'legal_name__c', 'name', 'email']].drop_duplicates()
    acs = acs[acs['tempo_id__c'].notna()]
    print(len(acs))
    acs['tempo_id__c'] = acs['tempo_id__c'].astype(str).apply(lambda x: x.split('.')[0])
    tempo_acs['tempo_id__c'] = tempo_acs['tempo_id__c'].astype(str).apply(lambda x: x.split('.')[0])

    xy = acs.merge(tempo_acs, how='left', left_on='tempo_id__c', right_on='tempo_id__c_x')
    # xy = xy[xy['test'].isnull()]
    # print(len(xy))
    # xy[['tempo_id__c', 'id', 'name_x', 'name_y']].to_csv('chelou_accounts.csv', index=False)

    print(acs['tempo_id__c'])
    tempo_acs = tempo_acs.merge(acs[['id', 'tempo_id__c']], how='left', on="tempo_id__c")
    tempo_acs = tempo_acs[tempo_acs['id'].isnull()]
    tempo_acs.to_csv("missing_tempo_accounts.csv", index=False)
    print(len(tempo_acs))