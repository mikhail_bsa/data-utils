SELECT DISTINCT
p.nom "Acolad_Entity",
t.nom "Vendor_Name",
tc.email "Vendor_Email",
tc.prenomcontact "Vendor_FirstName",
tc.nomcontact "Vendor_LastName",
CASE 
    WHEN ff.notvaintracomm is NULL THEN 'NA'
    WHEN ff.notvaintracomm = '' THEN 'NA'
    ELSE ff.notvaintracomm END "Vendor_VAT_Number",
t.adresse "Vendor_Address",
t.codepostal "Vendor_Postal_Code",
t.ville "Vendor_City", 
tp.paysL2 "Vendor_Country",
t.comptefournisseur "Vendor_Accounting_Code",
t.nourssaf "Vendor_Urssaf",
ff.tauxtva "Vendor_Vat_Regime",
ff.numfacture "Vendor_Invoice_Number",
ff.montantht "Vendor_Invoiced_Amount",
ff.datefacture "Vendor_Invoice_Date",
rf.datereglement "Vendor_Invoice_Payment_Date",
ff.regle "Vendor_PaymentStatus",
d.abbreviation "Vendor_Invoice_Currency",
com.numcommande "Project_Number",
CASE 
    WHEN lc.reference is NULL THEN 'NA'
    WHEN lc.reference = '' THEN 'NA'
    ELSE lc.reference END "Vendor_Task_Reference",
lc.prixfournisseur "Vendor_Task_Amount",
prestations.Prestation "Vendor_Task_Type",
c.nomentreprise "Client_Name",
f.nomclient "Client_Legal_Name",
f.pays "Client_Invoice_Country",
f.ville "Client_Invoice_City",
f.cp "Client_Invoice_Postcode",
p3.paysL2 "Client_Country_2",
f.compteclient "Client_Accounting_Code"
FROM clients c
LEFT JOIN commandes "com" on com.noclient = c.noclient
LEFT JOIN lignecommande lc on com.nocommande = lc.nocommande
LEFT JOIN contacts co on co.nocontact = com.nocontact
LEFT JOIN prestations on prestations.noprestation = lc.noprestation
LEFT JOIN Tradatp as "t" on t.notradatp = lc.notradatp
LEFT JOIN tradatpcontact "tc" on tc.notradatp = t.notradatp
LEFT JOIN pays "tp" on tp.nopays = t.nopays
LEFT JOIN parametres p on p.noparametre = lc.noparametre
LEFT JOIN facturefourncommande ffc on ffc.noligne = lc.noligne
LEFT JOIN devises d on d.nodevise = p.nodevise
LEFT JOIN facturesfourn ff on ff.nofacture = ffc.nofacture
LEFT JOIN devises d2 on d2.nodevise = ff.nodeviseconverti
LEFT JOIN facturecommande fc on fc.noligne = lc.noligne
LEFT JOIN factures f on fc.nofacture = f.nofacture
LEFT JOIN clientsadresses ca ON ca.noadresse = f.noadresse
LEFT JOIN pays p3 on p3.nopays = ca.nopays
LEFT JOIN TradATPComptesCharges tacc on (tacc.noparametre = lc.noparametre AND tacc.notradatp = t.notradatp)
LEFT JOIN facturefournreglement ffr on ffr.nofacture = ff.nofacture
LEFT JOIN Reglementfourn rf ON rf.noreglement = ffr.noreglement
WHERE ff.datefacture >= '2018-01-01' AND tc.principal = 2 and ff.nofacture is not NULL