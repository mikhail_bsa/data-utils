SELECT 
c.contactId "tempo_id__c",
c.contactId "legacy_id__c",
cfe.Id "accountId",
c.first_name "firstname",
c.last_name "lastname",
'TF' "acolad_company__c",
e.description "entity",
'Other - Imported' "leadsource",
'Client' "status__c",
cfe.PotentialCurrency "currencyisocode",
c.phone,
c.Mobile,
c.email,
c.exact_function "jobtitle",
t.Description "salutation",
l.Description "interaction_language__c",
'Other - imported' "lead_source__c"
FROM TEMPO.dbo.CustomerForEntity cfe 
LEFT JOIN TEMPO.dbo.contact c on c.CustomerForEntityId = cfe.Id
LEFT JOIN TEMPO.dbo.Title t on t.TitleId = c.TitleId
LEFT JOIN TEMPO.dbo.ContactRole	cr on cr.ContactRoleId = c.ContactRoleId
LEFT JOIN TEMPO.dbo.ContactDepartment cd on cd.Id = c.ContactDepartmentId
LEFT JOIN TEMPO.dbo.Language l on l.languagecode = c.languagecode
LEFT JOIN TEMPO.dbo.Entity e on e.entityId = c.entityId
WHERE c.email is not NULL AND c.contactId in {list};