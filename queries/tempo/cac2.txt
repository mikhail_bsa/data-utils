SELECT 
'TEMPO TLF' "legacy_erp__c",
cfe.Id "legacy_id__c",
cac.name_legal "legal_name__c",
cac.accountingcode "name",
cac.validityenddate "validity_end_date__c",
cac.id "tempo_id__c",
vr.Description "vat_regime__c",
pt.Description "payment_terms__c",
e.Description "acolad_company__c",
e.currencycode "currencyisocode"
from TEMPO.dbo.CustomerForEntity cfe
LEFT JOIN TEMPO.dbo.CustomerAccountingCode cac on cac.CustomerForEntityId = cfe.Id
LEFT JOIN TEMPO.dbo.PaymentTerms pt on pt.PaymentTermsId = cac.PaymentTermsId
LEFT JOIN TEMPO.dbo.VatRegime vr on vr.VatRegimeId = cac.VatRegimeId
LEFT JOIN TEMPO.dbo.Entity e on e.entityId = cac.entityId