import pandas as pd
import datetime as dt
from dateutil.relativedelta import relativedelta

from lbs import lbs_connection, concat_hl_tech
from utils import execute_query_file, send_email

if __name__ == '__main__':
    tech = lbs_connection(False)
    hl = lbs_connection(True)
    # Execute a query on one LBS DB
    df = execute_query_file(hl, "queries/lbs/compta.txt")
    
    # Execute a query on the two LBS DBs, with formatted args
    date2 = (dt.datetime.today()).strftime("%Y%m") + '01'
    date1 = (dt.datetime.today() - relativedelta(months=1)).strftime("%Y%m") + '01'
    df = concat_hl_tech(tech, hl, "queries/lbs/compta2.txt", format_args_hl={"date1": date1, "date2": date2},  format_args_tech={"date1": date1, "date2": date2})

    # Export to CSV Files
    df.to_csv('lbs_invoice2.csv', index=False, encoding='utf-8')

    # send file(s) by email
    send_email(
        '',
        'LBS Extract Invoice/Credit Notes',
        ["lbs_invoice2.csv"],
        ['dtaoufik@acolad.com', 'gpautonnier@acolad.com', 'nrabhi@acolad.com', 'mbessa@acolad.com']
    )
