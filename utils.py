# -*- coding: utf-8 -*-

import pandas as pd
import json
import numpy as np
import json_log_formatter
import logging
import requests
import sys
import pypyodbc
import math
import smtplib
import email.utils
from email.mime.text import MIMEText
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.utils import COMMASPACE, formatdate


CONFIG_FILEPATH = "config.json"
config = json.load(open(CONFIG_FILEPATH, 'r'))


def execute_query_pd(connection, query_filepath, args):
    try:
        with open(query_filepath, 'r') as f:
            query = f.read()
            logger.info("executing query {}...".format(query_filepath))
            df = pd.read_sql(query, connection, *args)
            logger.info("Done.")
            return df
    except:
        e = sys.exc_info()[0]
        logger.error(e)
        return

def execute_query_file(connection, query_filepath, format_args={}):
    try:
        with open(query_filepath, 'r') as f:
            query = f.read().format(**format_args)
            cursor = connection.cursor()
            cursor.execute(query)
            columns = [column[0] for column in cursor.description]
            df = pd.DataFrame(cursor.fetchall(), columns=columns)
            logger.info("query: {}, db: {}".format(query_filepath, connection.getinfo(pypyodbc.SQL_DATABASE_NAME)),
                extra={"type": "DB query", "DB": connection.getinfo(pypyodbc.SQL_DATABASE_NAME)})            
            return df
    except:
        e = sys.exc_info()
        logger.error("query: {}, db: {}, error: {}".format(query_filepath, connection.getinfo(pypyodbc.SQL_DATABASE_NAME), e), extra={"level": "error", "type": "DB query"})
        return

def execute_query_str(connection, query):
    try:
        cursor = connection.cursor()
        logger.info("executing query...")
        cursor.execute(query)
        logger.info("Success.")
        columns = [column[0] for column in cursor.description]
        df = pd.DataFrame(cursor.fetchall(), columns=columns)
        return df
    except:
        e = sys.exc_info()[0]
        logger.error(e)
        return

def get_connection(connection_str):
    try:
        connection = pypyodbc.connect(connection_str)
        return connection, True
    except pypyodbc.Error as err:
        logger.error('connection to DB failed: {}'.format(err), extra={'type': 'DB connection', "level": "error"})
        return None, False

def format_list(id_list):
    s = '('
    for i in range(len(id_list)):
        s += str(int(id_list[i]))
        if i < len(id_list) - 1:
            s += ', '
    s += ')'
    return s

def map_record(record, mapping, sf_object, ref=None):
    mapped_record = { "attributes": {"type": sf_object} }
    if ref is not None:
        mapped_record['attributes']['referenceId'] = ref
    if mapping is not None:
        for k, v in mapping.items():
            mapped_record[k] = record.get(v, None)
    else:
        for k, v in record.items():
            mapped_record[k] = v
    return mapped_record

class CustomHandler(logging.Handler):
    def __init__(self, service, source):
        self.url = config['DATADOG']['ENDPOINT'].format("THOR2", source, service)
        self.key = config['DATADOG']['API_KEY']
        super().__init__()

    def emit(self, record):
        log_entry = self.format(record)
        r = requests.post(self.url, log_entry,
            headers={
                "Content-type": "application/logplex-1",
                "DD-API-KEY": self.key
            }
        ).content

def getLogger(service, source, name=__name__):
    formatter = json_log_formatter.JSONFormatter()
    json_handler = CustomHandler(service=service, source=source)
    json_handler.setFormatter(formatter)
    json_handler.setLevel('INFO')
    logger = logging.getLogger(name)
    logger.setLevel('INFO')
    logger.addHandler(json_handler)
    formatter = json_log_formatter.JSONFormatter()
    return logger

def batch_function(function, args, array, batch_size=None, bs=50, auto=False, len_fun=None):
    n = len(array)
    if batch_size is None and n > 0:
        sizes = np.array(list(map(len_fun, array)))
        ind = np.argsort(sizes)[::-1]
        sizes = sizes[ind]
        array = np.array(array)[ind]
        beg, i, n_objects = 0, 0, 0
        while i <= n: 
            if i == n or (n_objects + sizes[i]) > bs:
                args['record_list'] = array[beg:i]
                args["batch_index"] = i
                args["n_batch"] = n
                if i != beg:
                    function(**args)
                beg, n_objects = i, 0
            if i < n:
                n_objects = n_objects + sizes[i]
            i += 1
    else:
        n_batch = math.ceil(len(array) / batch_size)
        for i in range(0, n, batch_size):
            args['record_list'] = array[i: min(i + batch_size, n)]
            args["batch_index"] = int(i / batch_size) + 1
            args["n_batch"] = n_batch
            function(**args)


def send_email(text, subject, files=[], receivers=["mbessa@acolad.com"]):
    # Create the message
    msg = MIMEMultipart()
    msg['To'] = COMMASPACE.join(receivers)
    msg['From'] = email.utils.formataddr(('Author', 'mbessa@acolad.com'))
    msg['Subject'] = subject
    msg.attach(MIMEText(text))
    for f in files:
        with open(f, "rb") as fil:
            # csv = MIMEText(fil.read())
            part = MIMEApplication(
                fil.read(),
                Name=basename(f)
            )
            part.add_header("Content-Disposition", "attachment",
               filename=basename(f))
        # After the file is closed
            msg.attach(part)
    server = smtplib.SMTP("smtp.office365.com", 587)
    try:
        server.ehlo()
        server.starttls()
        server.login('mbessa@acolad.com', 'Sisilaf@mill3')
        server.sendmail('mbessa@acolad.com', receivers, msg.as_string())
    finally:
        server.quit()

logger = getLogger(service='lbs_update', source="python_scripts")

