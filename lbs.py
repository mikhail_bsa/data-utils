import json
import requests
import pypyodbc
import pandas as pd
import json_log_formatter
import logging
import numpy as np
from utils import execute_query_file, get_connection, execute_query_pd, execute_query_str, format_list, map_record, getLogger

CONFIG_FILEPATH = "config.json"
config = json.load(open(CONFIG_FILEPATH, 'r'))
config_lbs = config['LBS']

logger=getLogger(service='lbs_update', source="python_scripts", name=__name__)

mappings = {
    "languages": pd.read_csv('mappings/sf_fields/languages.csv', encoding='latin-1', index_col=False),
    "owners": pd.read_csv("mappings/sf_fields/users.csv", encoding='latin-1',  index_col=False),
    "acolad_companies": pd.read_csv("mappings/sf_fields/acolad_companies.csv", index_col=False, encoding='latin-1'),
    "vat_regimes": pd.read_csv("mappings/sf_fields/vat_regimes.csv", index_col=False),
    "payment_terms": pd.read_csv("mappings/sf_fields/payment_terms.csv", index_col=False, encoding='latin-1'),
    "countries": pd.read_csv("mappings/sf_fields/countries.csv", index_col=False,  encoding='latin-1'),
    "prod_contacts": pd.read_csv("mappings/sf_fields/prod_contacts.csv", index_col=False,  encoding='latin-1'),
    "units": pd.read_csv("mappings/sf_fields/units.csv", index_col=False, encoding='latin-1'),
    "service_types": pd.read_csv("mappings/sf_fields/service_types.csv", index_col=False, encoding='latin-1')
}

def lbs_connection(hl=True):
    cnct_str = 'DRIVER={HFSQL}' + ';Server Name={};Server Port={};Database={};UID={};PWD={}'.format(
        config_lbs["SERVER"],config_lbs["PORT"], 
        config_lbs["HLTRAD_DB"] if hl else config_lbs['TECHNICIS_DB'], 
        config_lbs["USERNAME"], config_lbs["PASSWORD"]
    )
    logger.info('connecting to LBS {} DB...'.format('HLTRAD' if hl else 'TECHNICIS'), extra={"type": "DB connection"})
    connection, res = get_connection(cnct_str)
    if res:
        return connection


def concat_hl_tech(cnct_hl, cnct_tech, query_filepath, format_args_hl={}, format_args_tech={}):
    format_args_hl["origin"] = 'LBS HLTRAD'
    df_hl = execute_query_file(cnct_hl, query_filepath, format_args_hl)
    format_args_tech["origin"] = 'LBS TECHNICIS'
    df_tech = execute_query_file(cnct_tech, query_filepath, format_args_tech)
    return pd.concat([df_tech, df_hl])


def map_fields(df, dic):
    for k, v in dic.items():
        df = df.merge(mappings[v], how='left', left_on=k, right_on=mappings[v].columns[0])
        if k == 'ref':
            df['owner_id'].fillna(config['SF'][config['ENV']]['DEFAULT_OWNER'], inplace=True)
    return df 

def map_contacts(contacts):
    contacts['acolad_company'] = contacts['acolad_company'].str.lower()
    contacts['ref'] = contacts['owner'].apply(lambda x: x.split('@')[0].lower().strip() if x is not None else None)
    contacts = map_fields(contacts, {'ref': 'owners', 'acolad_company': 'acolad_companies', 'language': 'languages'})
    return contacts.replace({np.nan: None})

def map_lc(lc):
    lc['contact'] = lc['id_y']
    return lc

def map_accounts(accounts):
    accounts['acolad_company'] = accounts['acolad_company'].str.lower()
    accounts['ref'] = accounts['owner'].apply(lambda x: x.split('@')[0].lower().strip() if x is not None else None)
    accounts['prod_contact'] = accounts['prod_contact'].apply(
        lambda x: str(x).lower().strip()).str.normalize('NFKD').str.encode('ascii', errors='ignore'
        ).str.decode('latin-1')
    return map_fields(
        accounts,
        {'ref': 'owners', 'prod_contact': 'prod_contacts',
        'acolad_company': 'acolad_companies', 'country': 'countries', 'vat_regime': 'vat_regimes',
        'payment_terms': 'payment_terms'} 
    )

def id_col(df, id_col, erp_col='origin'):
    return df[erp_col].map({'TEMPO TLF': 'TLF', 'LBS TECHNICIS': 'TCH', 'LBS HLTRAD': 'HLT'}) + df[id_col].apply(str)

def map_opps(opps):
    print(len(opps))
    opps['acolad_company'] = opps['acolad_company'].str.lower()
    opps['id'] = id_col(opps, 'nocommande', 'origin')
    opps['datevalidation'].fillna(opps['datecommande'], inplace=True)
    opps['ref'] = opps['owner'].apply(lambda x: str(x).split('@')[0].lower().strip() if x is not None else None)
    opps['datevalidation'] = pd.to_datetime(opps['datevalidation'].astype(str), format="%Y%m%d", errors='ignore').apply(lambda x: x.strftime('%Y-%m-%d'))
    opps['datecommande'] = pd.to_datetime(opps['datecommande'].astype(str), format="%Y%m%d", errors='ignore').apply(lambda x: x.strftime('%Y-%m-%d'))
    opps['legacy_erp__c'] = opps['origin']
    opps['close_date'] = opps['datevalidation']
    return map_fields(opps, {'acolad_company': 'acolad_companies', 'ref': 'owners'}).drop_duplicates(['origin', 'nocommande'])

def map_pol(pol_purchase, pol_sales):
    return map_fields(
        pol_purchase, {
            "prestation": "service_types",
            "unit": "units",
            "source": "languages",
            "target":"languages",
    }), map_fields(
        pol_sales, {
            "prestation": "service_types",
            "unit": "units",
            "source": "languages",
            "target":"languages",
    }).drop_duplicates('id')

def process_contacts(contacts, sf_contacts, sf_lc, sf_ac):
    missing_contacts = contacts[contacts['id'].isnull()]
    missing_contacts = missing_contacts.merge(sf_ac[['account__c', 'legacy_id__c', 'legacy_erp__c']], how='left', left_on=['origin', 'noadresse'], right_on=['legacy_erp__c', 'legacy_id__c'] )
    missing_contacts = missing_contacts[missing_contacts['account__c'].notna()]
    missing_contacts.rename(columns={'account__c':'accountid'}, inplace=True)

    missing_contacts['email'] = missing_contacts['email'].apply(lambda x: x.lower().strip().split(';')[0])
    sf_contacts['email'] = sf_contacts['email'].str.lower().str.strip()
    missing_contacts = missing_contacts.merge(sf_contacts[['id', 'email', 'legacy_id__c', 'legacy_erp__c']], how='left', on='email')
    existing_mail = missing_contacts[missing_contacts['id_y'].notna()]
    missing_contacts = map_contacts(missing_contacts[missing_contacts['id_y'].isnull()])
    existing_mail = map_lc(existing_mail)
    missing_contacts['id'] = missing_contacts['origin'] + missing_contacts['noadresse'].astype(float).astype(str)
    existing_mail['id'] = existing_mail['origin'] + existing_mail['noadresse'].astype(float).astype(str)
    acc_to_fi = existing_mail[['origin', 'noadresse', 'id_y']].drop_duplicates()

    return missing_contacts.T.to_dict().values(), existing_mail.T.to_dict().values(), acc_to_fi

def process_accounts(accounts, acc_to_fi):
    accounts['id'] = id_col(accounts, 'noadresse')
    acc_to_fi['id'] = id_col(acc_to_fi, 'noadresse')
    
    fi = accounts[accounts['id'].isin(acc_to_fi['id'])]
    new_accounts = accounts[~accounts['id'].isin(acc_to_fi['id'])]

    print('ACCOUNT_LENGTH: ', len(fi), len(new_accounts))
    
    # fi = process_fi(fi, sf_contacts)

    new_accounts = map_accounts(new_accounts)
    new_accounts = new_accounts.T.to_dict().values()
    fi_mapping = json.load(open('mappings/sf_objects/accounting_code__c.json', 'r'))
    for r in new_accounts:
        r['accounting_codes'] = {"records": [map_record(r, fi_mapping, 'ACCOUNTING_CODE__C', r['id'] + 'FI')]}
    return new_accounts, fi

def process_fi(fi, contacts):
    return

def process_opps(missing_orders, sf_contacts):
    missing_orders = missing_orders.merge(sf_contacts[['legacy_erp__c', 'id', 'accountid', 'legacy_id__c']], how='left',  left_on=['origin', 'nocontact'], right_on=['legacy_erp__c', 'legacy_id__c'])
    missing_orders['datevalidation'].fillna(missing_orders['datecommande'], inplace=True)

def process_pols(pol):
    pol['id_co'] = id_col(pol, "nocommande")
    sales_cols = ["origin", "id_co", "nocommande", "ref_ligne", "noligne", "prixclient", "unitecl", "prestation", "tarifclientretenu1", "source", "target", "nombreuniteclient", "currencyisocode"]
    purchase_cols = ["origin", "id_co", "nocommande", "noligne", "ref_ligne", "prixfournisseur", "unitef",  "prestation", "tariffournretenu1", "source", "target", "nombreunitefourn", "currencyisocode"]
    pol_sales = pol[sales_cols][(pol['prixclient'] != 0) & pol['prixclient'].notna()]
    pol_purchase = pol[purchase_cols][(pol['prixfournisseur'] != 0) & (pol['prixfournisseur'].notna())] 
    pol_sales['type'] = "Sales"
    pol_purchase['type'] = "Purchase"
    pol_purchase['id'] = id_col(pol_purchase, "noligne") + 'P'
    pol_sales['id'] = id_col(pol_sales, "noligne") + 'S'

    pol_purchase.rename(columns={'prixfournisseur': 'amount', "unitef": "unit", "nombreunitefourn": "quantity", "tariffournretenu1": "rate"}, inplace=True)
    pol_sales.rename(columns= {'prixclient': 'amount', "unitecl": "unit", "nombreuniteclient": "quantity", "tarifclientretenu1": "rate"}, inplace=True)
    pol_purchase, pol_sales = map_pol(pol_purchase, pol_sales)
    pol = pd.concat([pol_sales, pol_purchase])
    pol.replace(np.nan, None, inplace=True)
    pol.drop_duplicates(subset=['id'], inplace=True)
    pol = pol.groupby('id_co')
    return pol


if __name__ == '__main__':
    c = lbs_connection(hl=True)
    # f = execute_query_file(c, "queries/lbs/compta.txt")
    # f.to_csv('factures.csv', index=False)